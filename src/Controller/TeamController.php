<?php

namespace App\Controller;

use App\Model\Team;


class TeamController
{
    public function __invoke()
    {
        return view('team', ['team' => Team::all()]);
    }


}
