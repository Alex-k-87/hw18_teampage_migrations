@extends('layouts.layout')

@section('content')

    <section
            class="page_breadcrumbs ds background_cover background_overlay section_padding_top_65 section_padding_bottom_65">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="highlight">Team type 1</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="ls page_portfolio section_padding_top_150 section_padding_bottom_130">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="isotope_container isotope row masonry-layout columns_margin_bottom_20">
                        @foreach($team as $elem)
                            <div class="isotope-item col-lg-4 col-md-6 col-sm-12">

                                <div class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
                                    <div class="item-media">
                                        <img src="images/team/01.jpg" alt=""/>
                                    </div>
                                    <div class="item-content">
                                        <p class="small-text highlight3 bottommargin_0">{{$elem->position}}</p>
                                        <h4 class="topmargin_0 hover-color3">
                                            <a href="team-single.html">{{$elem->name}}</a>
                                        </h4>
                                        <p>{{$elem->subscription}}</p>
                                        <p class="color2links">
                                            <a href="{{$elem->fblink}}" class="social-icon soc-facebook"></a>
                                            <a href="{{$elem->ttlink}}" class="social-icon soc-twitter"></a>
                                            <a href="{{$elem->glink}}" class="social-icon soc-google"></a>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <!-- eof .isotope_container.row -->

                </div>
            </div>
        </div>
    </section>
@endsection