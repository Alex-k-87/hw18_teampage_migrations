<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('teams', function ($table){
    $table->bigIncrements('id');
    $table->string('position', 100);
    $table->string('name', 100);
    $table->string('subscription', 255);
    $table->string('fblink', 100);
    $table->string('ttlink', 100);
    $table->string('glink', 100);
    $table->timestamps();
});

$data = [
    ['position' => 'DIRECTOR', 'name' => 'Theresa Green', 'subscription' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'fblink' => 'www.facebook.com', 'ttlink' => 'www.tweater.com', 'glink' => 'www.google.com' ],
    ['position' => 'CAT GROOMER', 'name' => 'Rosie White', 'subscription' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'fblink' => 'www.facebook.com', 'ttlink' => 'www.tweater.com', 'glink' => 'www.google.com' ],
    ['position' => 'DOG GROOMER', 'name' => 'Estelle Marsh', 'subscription' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
    'fblink' => 'www.facebook.com', 'ttlink' => 'www.tweater.com', 'glink' => 'www.google.com' ],
    ['position' => 'RABBIT GROOMER', 'name' => 'Theresa Green', 'subscription' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'fblink' => 'www.facebook.com', 'ttlink' => 'www.tweater.com', 'glink' => 'www.google.com' ],
    ['position' => 'WOLF GROOMER', 'name' => 'Theresa Green', 'subscription' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'fblink' => 'www.facebook.com', 'ttlink' => 'www.tweater.com', 'glink' => 'www.google.com' ],
    ['position' => 'RABBIT GROOMER', 'name' => 'Theresa Green', 'subscription' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'fblink' => 'www.facebook.com', 'ttlink' => 'www.tweater.com', 'glink' => 'www.google.com' ]

];

foreach ($data as $team) {
    $model = new \App\Model\Team();
    $model->position = $team['position'];
    $model->name = $team['name'];
    $model->subscription = $team['subscription'];
    $model->fblink = $team['fblink'];
    $model->ttlink = $team['ttlink'];
    $model->glink = $team['glink'];
    $model->save();
}